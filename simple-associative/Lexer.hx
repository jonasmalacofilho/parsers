enum Token {
	TParensOpen;
	TParensClose;
	TMinus;
	TAsterisc;
	TInteger(i:String);
}

class Lexer {
	public static function lex(str:String)
	{
		var buf = [];
		while (str.length > 0) {
			switch str.charAt(0) {
			case " ", "\t", "\n":
				// noop
				str = str.substr(1);
			case "(":
				buf.push(TParensOpen);
				str = str.substr(1);
			case ")":
				buf.push(TParensClose);
				str = str.substr(1);
			case "-":
				buf.push(TMinus);
				str = str.substr(1);
			case "*":
				buf.push(TAsterisc);
				str = str.substr(1);
			case _:
				var r = ~/[0-9]+/;
				if (r.match(str)) {
					buf.push(TInteger(r.matched(0)));
					str = r.matchedRight();
				} else {
					throw 'Unknow stuff: $str';
				}
			}
		}
		return buf;
	}
}

