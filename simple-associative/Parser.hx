import Lexer;

enum Factor {
	FExpr(e:Expr);
	FInt(i:Int);
}

enum Term {
	TMulti(a:Factor, b:Factor);
	TFactor(f:Factor);
}

enum Expr {
	ESub(a:Term, b:Term);
	ETerm(t:Term);
}

class Parser {
	var tokens:Array<Token>;

	function new(tokens)
	{
		this.tokens = tokens;
	}

	function parseFactor()
	{
		if (tokens[0] == TParensOpen) {
			tokens.shift();
			var expr = parseExpr();
			if (tokens.shift() != TParensClose) throw "Assert failed";
			return FExpr(expr);
		} else {
			var v = switch tokens.shift() {
			case TInteger(lit): Std.parseInt(lit);
			case o: throw 'Assert failed: expected TInteger but was $o';
			}
			return FInt(v);
		}
	}

	function parseTerm()
	{
		var factor = parseFactor();
		if (tokens[0] == TAsterisc) {
			tokens.shift();
			var by = parseFactor();
			return TMulti(factor, by);
		}
		return TFactor(factor);
	}

	function parseExpr()
	{
		var term = parseTerm();
		if (tokens[0] == TMinus) {
			tokens.shift();
			var sub = parseTerm();
			return ESub(term, sub);
		}
		return ETerm(term);
	}

	public static function parse(str:String)
	{
		var tokens = Lexer.lex(str);
		return new Parser(tokens).parseExpr();
	}
}

