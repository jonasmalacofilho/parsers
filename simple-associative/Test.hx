import Lexer.lex;
import Lexer;
import Parser.parse;
import Parser;
import utest.Assert;

class Test {
	function new() {}

	function test_000_noop()
	{
		Assert.isTrue(true);
	}

	function test_001_lex()
	{
		Assert.same([TMinus, TAsterisc, TParensOpen, TInteger("123"), TParensClose], lex("-   *( 123 \t \n)"));

		Assert.same([TInteger("1"), TMinus, TInteger("2"), TMinus, TInteger("3")], lex("1 - 2 - 3"));
	}

	function test_002_parse()
	{
		// broken
		Assert.same(ESub(TFactor(FInt(1)), TFactor(FInt(2))), parse("1 - 2 - 3"));
	}

	static function main()
	{
		var u = new utest.Runner();
		u.addCase(new Test());
		utest.ui.Report.create(u);
		u.run();
	}
}

