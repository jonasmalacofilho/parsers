typedef Position = {
	min:Int,
	max:Int
}

enum Binop {
	OAddition;
	OSubstraction;
	OProduct;
	ODivision;
	OModulo;
}

enum ExprDef {
	ENumber(s:String);
	EBinop(op:Binop, e1:Expr, e2:Expr);
}

typedef Expr = {
	def : ExprDef,
	pos : Position
}

