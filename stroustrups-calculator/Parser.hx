import Expr;
import Token;
using TokenTools;

class Parser {
	var lex:Lexer;
	var queue:Null<Token>;

	public function new(lex)
		this.lex = lex;

	public function parse()
	{
		var expr = parseExpr();
		switch next() {
		case TEof:  // ok
		case other: throw error('Unexpected ${other.toString()}');
		}
		return expr;
	}

	/*
	Expressions are

		expr ::= expr + term
		expr ::= expr - term
		expr ::= term

	but, to avoid left recursion, we implement

		expr ::= term rexpr
		rexpr ::= + term rexpr
		rexpr ::= - term rexpr
		rexpr ::= empty
	*/
	function parseExpr()
	{
		var term = parseTerm();
		return parseRestExpr(term);
	}

	function parseRestExpr(left)
	{
		switch next() {
		case TPlus:
			var right = parseTerm();
			return parseRestExpr(make(EBinop(OAddition, left, right)));
		case TMinus:
			var right = parseTerm();
			return parseRestExpr(make(EBinop(OSubstraction, left, right)));
		case other:
			push(other);
			return left;
		}
	}

	/*
	Terms are

		term ::= term * primary
		term ::= term / primary
		term ::= term % primary
		term ::= primary

	but, to avoid left recursion, we implement

		term ::= primary rterm
		rterm ::= * primary rterm
		rterm ::= / primary rterm
		rterm ::= % primary rterm
		rterm ::= empty
	*/
	function parseTerm()
	{
		var left = parsePrimary();
		return parseRestTerm(left);
	}

	function parseRestTerm(left)
	{
		switch next() {
		case TAsterisc:
			var right = parsePrimary();
			return parseRestTerm(make(EBinop(OProduct, left, right)));
		case TSlash:
			var right = parsePrimary();
			return parseRestTerm(make(EBinop(ODivision, left, right)));
		case TPercent:
			var right = parsePrimary();
			return parseRestTerm(make(EBinop(OModulo, left, right)));
		case other:
			push(other);
			return left;
		}
	}

	/*
	Primaries are

		primary ::= ( expr )
		primary ::= number
	*/
	function parsePrimary()
	{
		switch next() {
		case TParensOpen:
			var sub = parseExpr();
			switch next() {
			case TParensClose: // ok
			case _: throw error('Unclosed parens');
			}
			return sub;
		case TNumber(text):
			return make(ENumber(text));
		case other:
			throw error('Expected number or expression in parens, but got ${other.toString()}');
		}
	}

	function next()
	{
		if (queue != null) {
			var n = queue;
			queue = null;
			return n;
		} else {
			return lex.token();
		}
	}

	function push(token:Token)
	{
		if (queue != null) throw 'No place to store token $token; already keeping $queue';
		queue = token;
	}

	function error(msg:String, ?pos:Position)
	{
		if (pos == null) pos = lex.curPos();
		return 'bytes ${pos.min}-${pos.max}: $msg';
	}

	function span(left:Position, right:Position)
		return { min:left.min, max:right.max };

	function make(def:ExprDef)
	{
		var pos = switch def {
		case EBinop(_, left, right): span(left.pos, right.pos);
		case ENumber(_): lex.curPos();
		}
		return { def:def, pos:pos };
	}
}

