#if macro
import haxe.macro.Context;
import haxe.macro.Expr;
using haxe.macro.ExprTools;
#else
import Expr;
import Token;
import utest.Assert;
#end

class Test {
	static macro function posify(expr:Expr)
		return _posify(expr, { min:0, max:0 });

	// simplify parser tests
	static macro function assertParse(exp:Expr, got:Expr)
	{
		return macro @:pos(Context.currentPos()) {
			var exp = posify(@:pos(exp.pos) $exp);
			var got = parse(@:pos(got.pos) $got);
			// trace(exp);
			// trace(got);
			Assert.same(exp, got);
		}
	}

	// if the haxe expr and the calc expr are equal, don't write them twice
	static macro function assertEval(expr:Expr)
	{
		var text = expr.toString();
		return macro @:pos(Context.currentPos()) {
			// trace($v{text} + ": " + $expr);
			Assert.floatEquals($expr, eval($v{text}));
		}
	}
#if macro
	static function _posify(expr:Expr, pos:{ min:Int, max:Int })
	{
		switch expr.expr {
		case ECall({ expr:EConst(CIdent("ENumber")) }, [{ expr:EConst(CString(num)) }]):
			pos.min = pos.max;
			pos.max += num.length;
			return macro @:pos(Context.currentPos()) { def:$expr, pos:{ min:$v{pos.min}, max:$v{pos.max} } };
		case ECall({ expr:EConst(CIdent("EBinop")) }, [op, e1, e2]):
			e1 = _posify(e1, pos);
			var min = pos.min;  // save left/min
			pos.max++;  // every binop is at least 1-byte long
			e2 = _posify(e2, pos);
			pos.min = min;
			var e = macro @:pos(expr.pos) EBinop($op, $e1, $e2);
			return macro @:pos(Context.currentPos()) { def:$e, pos:{ min:$v{pos.min}, max:$v{pos.max} } };
		case _:
			return expr.map(_posify.bind(_, pos));
		}
	}
#else
	function new() {}

	function test_000_noop()
	{
		Assert.isTrue(true);
	}

	function tokens(str:String)
	{
		var lx = new Lexer(str);
		var ret = [];
		while (ret[ret.length - 1] != TEof)
			ret.push(lx.token());
		return ret;
	}

	function test_001_lexer()
	{
		// basic
		Assert.same([TEof], tokens(""));
		Assert.same([TEof], tokens(" \t\n"));
		Assert.same([TPlus,TEof], tokens("+"));
		Assert.same([TMinus,TEof], tokens("-"));
		Assert.same([TAsterisc,TEof], tokens("*"));
		Assert.same([TSlash,TEof], tokens("/"));
		Assert.same([TPercent,TEof], tokens("%"));
		Assert.same([TParensOpen,TEof], tokens("("));
		Assert.same([TParensClose,TEof], tokens(")"));
		Assert.same([TNumber("0"),TEof], tokens("0"));
		Assert.same([TNumber("123"),TEof], tokens("123"));
		Assert.same([TNumber("1.23"),TEof], tokens("1.23"));
		Assert.same([TNumber(".123"),TEof], tokens(".123"));
		Assert.same([TNumber("123e4"),TEof], tokens("123e4"));
		Assert.same([TNumber("123e+4"),TEof], tokens("123e+4"));
		Assert.same([TNumber("123e-4"),TEof], tokens("123e-4"));
		Assert.same([TNumber("123E4"),TEof], tokens("123E4"));
		Assert.same([TNumber("123E+4"),TEof], tokens("123E+4"));
		Assert.same([TNumber("123E-4"),TEof], tokens("123E-4"));

		// complex expressions
		Assert.same([TNumber("45"),TPlus,TNumber("11.5"),TSlash,TNumber("7"),TEof], tokens("45 + 11.5/7"));

		// exceptions
		Assert.raises(tokens.bind("1."));
		Assert.raises(tokens.bind(".1e2"));

		// delayed exceptions
		Assert.same([TNumber("0"),TNumber("1"),TEof], tokens("01"));  // let the parser handle this error later

		// always eof
		var lx = new Lexer("");
		Assert.same(TEof, lx.token());
		Assert.same(TEof, lx.token());
	}

	function parse(str:String)
	{
		var lx = new Lexer(str);
		var p = new Parser(lx);
		return p.parse();
	}

	function test_002_parser()
	{
		// basic
		assertParse(ENumber("0"), "0");
		assertParse(ENumber("1.23"), "1.23");
		assertParse(EBinop(ODivision, ENumber("1"), ENumber("23")), "1/23");
		assertParse(EBinop(OProduct, EBinop(ODivision, ENumber("1"), ENumber("23")), ENumber("45")), "1/23*45");
		assertParse(EBinop(OSubstraction, ENumber("1"), ENumber("23")), "1-23");
		assertParse(EBinop(OAddition, EBinop(OSubstraction, ENumber("1"), ENumber("23")), ENumber("45")), "1-23+45");
		assertParse(EBinop(OAddition, ENumber("45"), EBinop(ODivision, ENumber("11.5"), ENumber("7"))), "45+11.5/7");

		// position
		Assert.same({ def:ENumber("0"), pos:{ min:1, max:2 } }, parse(" 0"));

		// syntax errors
		Assert.raises(parse.bind("a"));
		Assert.raises(parse.bind("1 2"));
		Assert.raises(parse.bind("1++"));
		Assert.raises(parse.bind("01"));  // delayed from the lexer
	}

	function eval(str:String)
	{
		var i = new Interp();
		return i.eval(parse(str));
	}


	function test_003_interp()
	{
		Assert.floatEquals(45 + 11.5/7, eval("45 + 11.5/7"));
		assertEval(45 + 11.5/7);
		assertEval(3%2);
	}

	static function main()
	{
		var u = new utest.Runner();
		u.addCase(new Test());
		utest.ui.Report.create(u);
		u.run();
	}
#end
}

