class TokenTools {
	public static function toString(token:Token)
	{
		return switch token {
		case TEof: "[Eof]";
		case TPlus: "plus (+)";
		case TMinus: "minus (-)";
		case TAsterisc: "asterisc (*)";
		case TSlash: "slash (/)";
		case TPercent: "percent (%)";
		case TParensOpen: "opening parens";
		case TParensClose: "closing parens";
		case TNumber(s): s;
		}
	}
}

