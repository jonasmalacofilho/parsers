import Expr;

class Interp {
	function evalExpr(e:Expr)
	{
		switch e.def {
		case ENumber(text):
			return Std.parseFloat(text);
		case EBinop(op, e1, e2):
			var v1 = evalExpr(e1);
			var v2 = evalExpr(e2);
			return switch op {
			case OAddition: v1 + v2;
			case OSubstraction: v1 - v2;
			case OProduct: v1*v2;
			case ODivision: v1/v2;
			case OModulo: (Std.int(v1) % Std.int(v2):Float);
			}
		}
	}

	public function eval(e)
		return evalExpr(e);

	public function new() {}
}

