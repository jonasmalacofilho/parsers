import Token;

typedef Rule = {
	ereg : EReg,
	action : EReg -> Token
}

class Lexer {
	public var current(default,null):String;
	var rules:Array<Rule>;
	var buf:String;
	var pos:Int;

	function mkRule(pat, action)
		return { ereg : new EReg("^"+pat, ""), action : action };

	function mkCteAction(e:Token)
		return function (r) return e;

	public function curPos()
		return { min : pos - current.length, max : pos };

	public function token()
	{
		var best = null;
		var bestLength = 0;
		for (r in rules) {
			if (r.ereg.match(buf)) {
				if (best == null || r.ereg.matched(0).length > bestLength) {
					best = r;
					bestLength = r.ereg.matched(0).length;
				}
			}
		}
		if (best == null) throw 'Could not lex: $buf';
		current = best.ereg.matched(0);
		pos += current.length;
		buf = best.ereg.matchedRight();
		return best.action != null ? best.action(best.ereg) : token();
	}

	public function new(input)
	{
		var int = "((0)|([1-9][0-9]*))";
		rules = [
			mkRule("$", mkCteAction(TEof)),
			mkRule("[ \t\n\r]+", null),
			mkRule("\\+", mkCteAction(TPlus)),
			mkRule("-", mkCteAction(TMinus)),
			mkRule("\\*", mkCteAction(TAsterisc)),
			mkRule("/", mkCteAction(TSlash)),
			mkRule("%", mkCteAction(TPercent)),
			mkRule("\\(", mkCteAction(TParensOpen)),
			mkRule("\\)", mkCteAction(TParensClose)),
			mkRule(int, function (r) return TNumber(r.matched(0))),
			mkRule(int+"?\\.[0-9]+", function (r) return TNumber(r.matched(0))),
			mkRule(int+"(\\.[0-9]+])?[eE][+-]?[0-9]+", function (r) return TNumber(r.matched(0))),
		];
		buf = input;
		pos = 0;
	}
}

