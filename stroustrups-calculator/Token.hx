enum Token {
	TEof;
	TPlus;
	TMinus;
	TAsterisc;
	TSlash;
	TPercent;
	TParensOpen;
	TParensClose;
	TNumber(s:String);
}

